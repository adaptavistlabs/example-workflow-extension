package com.adaptavist.projectconfigurator.wes4jextension;

import com.atlassian.jira.issue.fields.Field;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.plugin.spring.scanner.annotation.Profile;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.awnaba.projectconfigurator.extensionpoints.common.HookPointCollection;
import com.awnaba.projectconfigurator.extensionpoints.common.ParamValueTranslator;
import com.awnaba.projectconfigurator.extensionpoints.extensionservices.ReferenceMarkerFactory;
import com.awnaba.projectconfigurator.extensionpoints.extensionservices.ReferenceOption;
import com.awnaba.projectconfigurator.extensionpoints.extensionservices.TranslatorFactory;
import com.awnaba.projectconfigurator.extensionpoints.workflow.WorkflowReferencePoint;
import com.awnaba.projectconfigurator.extensionpoints.workflow.WorkflowReferencePointImpl;
import com.awnaba.projectconfigurator.extensionpoints.workflow.WorkflowTranslationPoint;
import com.awnaba.projectconfigurator.extensionpoints.workflow.WorkflowTranslationPointImpl;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

@Profile("pc4j-extensions")
@Component
public class WES4JExtensionModule implements HookPointCollection {

	private TranslatorFactory translatorFactory;
	private ReferenceMarkerFactory referenceMarkerFactory;
	private FieldManager fieldManager;

	@Inject
	public WES4JExtensionModule(@ComponentImport TranslatorFactory translatorFactory,
								@ComponentImport ReferenceMarkerFactory referenceMarkerFactory,
								@ComponentImport FieldManager fieldManager) {
		this.translatorFactory = translatorFactory;
		this.referenceMarkerFactory = referenceMarkerFactory;
		this.fieldManager = fieldManager;
	}

	public WorkflowReferencePoint getAssignSpecificUserHookPoint() {

		return new WorkflowReferencePointImpl(
				referenceMarkerFactory.fromOption(ReferenceOption.USERNAME),
				"//function[arg[@name='class.name' and (text()='de.codecentric.jira.postfunction.SetAssigneeToSpecificUserPostFunction')]]/arg[@name='USERNAME_VALUE_FIELD']/text()"
		);
	}

	public WorkflowTranslationPoint getDateCompareConditionHookPoint() {

		ParamValueTranslator translator = translatorFactory.choiceOf(
				translatorFactory.fromOption(TranslatorFactory.TranslateOption.VOID_TRANSLATOR),
				translatorFactory.fromOption(TranslatorFactory.TranslateOption.CUSTOM_FIELD_ID),
				this::isSystemCustomFieldId, this::isSystemCustomFieldId);

		return new WorkflowTranslationPointImpl(
				translator,
				"//condition[arg[@name='class.name' and (text()='de.codecentric.jira.condition.DateComparisonCondition')]]/arg[@name='FIELD_ID']/text()"
		);
	}

	private boolean isSystemCustomFieldId(String fieldId) {
		Field field = fieldManager.getField(fieldId);
		return (field != null) && !fieldManager.isCustomField(field);
	}

}
